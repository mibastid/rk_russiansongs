<?php

set_time_limit(0);

$files = getAllSongsDirectory($argv[1]);

//var_dump($files);
$counterTotal = count($files);
$counterIndex = 1;
foreach ($files as $fileName) {
	playThread($fileName, $argv[1]);	
	echo $counterIndex++ . '/' . $counterTotal . "\r\n";
}


function getAllSongsDirectory($path){
	$files = array();
	foreach (glob("$path/dat/*.dat") as $file) {
		$file = explode('.dat', $file)[0];
		$file = end(explode('/', $file));
		$files[] = $file;
	}
	return $files;
}

function playThread($fileName, $path){
	generateScrFile($fileName,$path);
	generateLrcFile($fileName,$path);
	
}

function generateScrFile($fileName,$path){
	$lines = file("$path/dat/" . $fileName . '.dat');
	$myfile = fopen("$path/scr/" . $fileName . ".scr", "w");
	$bpm = getBpm($fileName, $path);
	$beatStartLine = 0;
	$beatEndLine = 0;

	fwrite($myfile, "#BPM:" . $bpm);
	fwrite($myfile, "\r\n");
	fwrite($myfile, "#GAP:0");
	fwrite($myfile, "\r\n");

	$totalLength = count($lines);
	foreach ($lines as $num_line => $line) {
		$line = explode("|", $line);
		$i = 0;
		$lineLength = count($line);
		if($lineLength > 1){
			foreach ($line as $num_sil => $sil) {
				$sil = explode("^", $sil);
				if(count($sil)>1){
					$beatStartLine = timeToBeat($sil[0], $bpm, 0);
					$beatEndLine = timeToBeat($sil[1], $bpm, 0);
					if(rtrim($sil[3] == '')){
						$sil[3] = '~';
					}
					$beatDuration = $beatEndLine-$beatStartLine;
					if($beatDuration <= 0){
						$beatDuration = 1;
					}
					$text = ': ' . $beatStartLine . ' ' . ($beatDuration) . ' ' . $sil[2] . ' ' . $sil[3];
					fwrite($myfile, $text);
				}
				$i++;
				if($i < $lineLength){
					fwrite($myfile, "\r\n");
				} else {
					if($num_line + 1 == $totalLength){
						fwrite($myfile, "\r\n");
					}
					fwrite($myfile, "- " . ($beatEndLine) . "\r\n");
				}
			}
		}
	}

	fwrite($myfile, "E");
	fclose($myfile);
	exec("dos2unix $path/scr/" . $fileName . ".scr" . ' 2>&1', $auxi1);
}


function generateLrcFile($fileName,$path){
	$lines = file("$path/dat/" . $fileName . '.dat');
	$myfile = fopen("$path/lrc/" . $fileName . ".lrc", "w");
	$j=0;
	$linesSize = count($lines);
	foreach ($lines as $num_line => $line) {
		$line = explode("|", $line);
		$i = 0;
		$lineLength = count($line);
		if($lineLength > 1){
			foreach ($line as $num_sil => $sil) {
				$sil = explode("^", $sil);
				if(count($sil)>1){
					$auxi = explode('.', $sil[0]);
					if(count($auxi)>1){
						$auxi = substr($auxi[1], 0, 3);
					}else{
						$auxi = '000';
					}
					$beatStartLine = gmdate("i:s", (int)$sil[0]) . '.' . $auxi;
					if($beatStartLine[0]==0){
						$beatStartLine = substr($beatStartLine, 1);
					}
					while(strlen($beatStartLine) < 8){
						$beatStartLine = $beatStartLine . '0';
					}
					$auxi = explode('.', $sil[1]);
					if(count($auxi)>1){
						$auxi = substr($auxi[1], 0, 3);
					}else{
						$auxi = '000';
					}
					$beatEndLine = gmdate("i:s", (int)$sil[1]) . '.' . $auxi;
					if($beatEndLine[0]==0){
						$beatEndLine = substr($beatEndLine, 1);
					}
					while(strlen($beatEndLine) < 8){
						$beatEndLine = $beatEndLine . '0';
					}
					if($beatEndLine < $beatStartLine){
						$auxi = explode(':', $beatStartLine);
						$auxi[1] = floatval($auxi[1])+0.100;
						$beatEndLine = $auxi[0].':'.$auxi[1];
						$sil[3] = '~';
					}
					if(rtrim($sil[3] == '')){
						$sil[3] = '~';
					}
					$text = $beatStartLine . '-' . $beatEndLine . '-' ;
					if($i+2 > $lineLength){ //si es la última sílaba
						$text = $text . rtrim($sil[3]) . '\\n';
					}else{
						$text = $text . $sil[3];
					}
					fwrite($myfile, $text);		
				}
				$i++;
				if($j+1 != $linesSize){
					fwrite($myfile, "\r\n");
				}elseif ($i != $lineLength ) {
					fwrite($myfile, "\r\n");
				}
			}
		}
		$j++;
	}
	fclose($myfile);
	exec("dos2unix $path/lrc/" . $fileName . ".lrc" . ' 2>&1', $auxi1);
}


function timeToBeat($time, $bpm, $gap){
	$time = $time * 1000;
	$duration = 1 / $bpm / 4 * 60 * 1000;
	return floor(($time - $gap) / $duration );
}


function getBpm($fileName,$path){
	//return 144; // bpm default
	// $fileName = "$path/mp3/" . $fileName . '.mp3';
	$fileName = "http://www.redkaraoke.es/singberry/" . $fileName . '.mp3';
	exec('bpm-tag -n ' . escapeshellarg($fileName) . ' 2>&1', $auxi1);
	$auxi1 = explode(" ", $auxi1[0]);
	$auxi1 = floatval($auxi1[count($auxi1)-2]);
	if($auxi1 == 0){
		$auxi1 = 144; // bpm default
	} 
	echo 'bpm: ' . $auxi1 . "\t- ";
	return $auxi1;
}

